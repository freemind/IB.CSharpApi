# Interactive Brokers - TWS API v9.72+ #

[![Build status](https://ci.appveyor.com/api/projects/status/kv0c8og3w2thep4f/branch/master?svg=true)](https://ci.appveyor.com/project/mathpaquette/ib-csharpapi/branch/master) [![NuGet Version and Downloads count](https://buildstats.info/nuget/IB.CSharpApi)](https://www.nuget.org/packages/IB.CSharpApi)  

This repository hosts the official C# TWS API code retrieved from the API package available from the [IB website](http://interactivebrokers.github.io/).  

The only purpose of it is to build and publish an untouched [NuGet](https://www.nuget.org/packages/IB.CSharpApi) package available to your project.
 
##### How to use  
`Install-Package IB.CSharpApi`  
